# target-mysql

This is a [Singer](https://singer.io) target that reads JSON-formatted data
following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md)
and loads them to MySQL.

This is a VERY basic implementation. At present, it will (usually) write data to destination tables, assuming they don't already exist, and assuming a few different bugs in the Meltano SDK have been fixed. It is really just a proof of concept and is definitely _not_ ready for production.

The almost total lack of code is intentional and is a testament to the amount of functionality included in the Meltano SDK.

## Installation

1. Create and activate a virtualenv
2. `pip install -e '.[dev]'`  

## Configuration of target-mysql

**config.json**
```json
{
  "host": "The host name of the MySQL server to connect to",

  "port": "The port to connect to the MySQL server on (Default=3306)",

  "user": "The user name to connect to MySQL as",
   
  "password": "The password for the given MySQL user",

  "dbname": "The name of the MySQL database to use"
}
```


