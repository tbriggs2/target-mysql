from singer_sdk import SQLTarget

from target_mysql.config import target_config
from target_mysql.sink import MySQLSink

DB_PATH_CONFIG = "path_to_db"


class MySQLTarget(SQLTarget):
    """The Target class for MySQL."""

    name = "target-mysql"
    default_sink_class = MySQLSink
    max_parallelism = 1

    config_jsonschema = target_config.to_dict()
