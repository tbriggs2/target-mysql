"""A sample implementation for MySQL."""

from singer_sdk import SQLStream

from target_mysql.connector import MySQLConnector


class MySQLStream(SQLStream):
    """The Stream class for MySQL.

    This class allows developers to optionally override `process_batch()` and other
    sink methods in order to improve performance beyond the default SQLAlchemy-based
    interface.

    DDL and type conversion operations are delegated to the connector logic specified
    in `connector_class` or by overriding the `connector` object.
    """

    connector_class = MySQLConnector
