from singer_sdk import typing as th

DB_HOST = "host"
DB_PORT = "port"
DB_USER = "user"
DB_PASSWORD = "password"
DB_NAME = "dbname"

target_config = th.PropertiesList(
    th.Property(
        DB_HOST,
        th.StringType,
        description="The host name of the MySQL server",
        required=True
    ),
    th.Property(
        DB_PORT,
        th.IntegerType,
        description="The port of the MySQL server",
        required=False,
        default=3306
    ),
    th.Property(
        DB_USER,
        th.StringType,
        description="The user name to connect as",
        required=True
    ),
    th.Property(
        DB_PASSWORD,
        th.StringType,
        description="The password for the given user",
        required=True
    ),
    th.Property(
        DB_NAME,
        th.StringType,
        description="The name of the database to connect to",
        required=True
    ),
)

__all__ = ["DB_HOST", "DB_PORT", "DB_USER", "DB_PASSWORD", "DB_NAME", "target_config"]
