from typing import Any, Dict

import sqlalchemy
from singer_sdk import SQLConnector

DB_HOST = "host"
DB_PORT = "port"
DB_USER = "user"
DB_PASSWORD = "password"
DB_NAME = "dbname"


class MySQLConnector(SQLConnector):
    """The connector for MySQL.

    This class handles all DDL and type conversions.
    """

    allow_temp_tables = True
    allow_column_alter = False
    allow_merge_upsert = True

    def get_sqlalchemy_url(self, config: Dict[str, Any]) -> str:
        """Generates a SQLAlchemy URL for MySQL."""
        return f"mysql+mysqldb://{config[DB_USER]}:{config[DB_PASSWORD]}@{config[DB_HOST]}:{config[DB_PORT]}/{config[DB_NAME]}"

    def create_sqlalchemy_connection(self) -> sqlalchemy.engine.Connection:
        """Return a new SQLAlchemy connection using the provided config.

        This override simply provides a more helpful error message on failure.

        Returns:
            A newly created SQLAlchemy engine object.
        """
        try:
            return super().create_sqlalchemy_connection()
        except Exception as ex:
            raise RuntimeError(
                f"Error connecting to DB "
            ) from ex

